# AATKit_Corona


## Overview

The aatkit plugin for Corona SDK offers easy integration of mobile ad networks. AddApptr gives you access to all the networks with one single SDK.

### You can download plugin [here](https://android-sdk.aatkit.com/deprecated/AATKit_Corona.zip)
### [Documentation](https://bitbucket.org/addapptr/aatkit-corona-sample/wiki/)
### [Changelogs](https://bitbucket.org/addapptr/aatkit-corona-sample/wiki/Changelogs)
### [AddApptr Account](https://addapptr.com/admin/)