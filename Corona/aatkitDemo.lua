-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
local scene = composer.newScene()

-- include Corona's "widget" library
local widget = require "widget"

-- include AATKit
local aatkit = require "com.intentsoftware.addapptr"

--------------------------------------------

-- forward declarations and other locals
local version = "unknown"
local bannerId = "BannerPlacement"
local interstitialId = "FullscreenPlacement"
local initialized = false
local shakeEnabled = false
local bannerAlignment = AATKitBannerAlignBottomCenter
local bannerAutoreload = false
local interstitialAutoreload = false

local initBtn
local debugBtn
local shakeBtn
local reloadBannerBtn
local autoreloadBannerBtn
local bannerAlignBtn
local reloadInterstitialBtn
local autoreloadInterstitialBtn
local showInterstitialBtn

local function onInitBtnRelease()
	if initialized == false then
		aatkit.initWithTestMode( libraryCallback, 74 )
		aatkit.createPlacement( bannerId, AATKitBannerAuto )
		aatkit.createPlacement( interstitialId, AATKitFullscreen )
		initialized = true
	end

	return true
end

local function onDebugBtnRelease()
	if initialized then
		aatkit.setDebugEnabled()
	end

	return true
end

local function onShakeBtnRelease()
	if initialized then
		if shakeEnabled then
			aatkit.setDebugShakeEnabled(false)
			shakeEnabled = false
			shakeBtn:setLabel( "Enable Debug Shake")
		else
			aatkit.setDebugShakeEnabled(true)
			shakeEnabled = true
			shakeBtn:setLabel( "Disable Debug Shake")
		end
	end

	return true
end

local function onReloadBannerBtnRelease()
	if initialized then
		aatkit.reloadPlacement( bannerId )
	end

	return true
end

local function onAutoreloadBannerBtnRelease()
	if initialized then
		if bannerAutoreload then
			aatkit.stopPlacementAutoReload( bannerId );
			bannerAutoreload = false
			autoreloadBannerBtn:setLabel( "Enable Banner Autoreload" )
		else
			aatkit.startPlacementAutoReload( bannerId);
			bannerAutoreload = true
			autoreloadBannerBtn:setLabel( "Disable Banner Autoreload" )
		end
	end

	return true
end

local function onBannerAlignBtnRelease()
	if initialized then
		if bannerAlignment == AATKitBannerAlignBottomCenter then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignBottomRight )
			bannerAlignment = AATKitBannerAlignBottomRight
		elseif bannerAlignment == AATKitBannerAlignBottomRight then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignBottomLeft )
			bannerAlignment = AATKitBannerAlignBottomLeft
		elseif bannerAlignment == AATKitBannerAlignBottomLeft then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignTopCenter )
			bannerAlignment = AATKitBannerAlignTopCenter
		elseif bannerAlignment == AATKitBannerAlignTopCenter then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignTopRight )
			bannerAlignment = AATKitBannerAlignTopRight
		elseif bannerAlignment == AATKitBannerAlignTopRight then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignTopLeft )
			bannerAlignment = AATKitBannerAlignTopLeft
		elseif bannerAlignment == AATKitBannerAlignTopLeft then
			aatkit.setPlacementAlignment( bannerId, AATKitBannerAlignBottomCenter )
			bannerAlignment = AATKitBannerAlignBottomCenter
		end
	end

	return true
end

local function onReloadInterstitialBtnRelease()
	if initialized then
		aatkit.reloadPlacement( interstitialId )
	end

	return true
end

local function onAutoreloadInterstitialBtnRelease()
	if initialized then
		if interstitialAutoreload then
			aatkit.stopPlacementAutoReload( interstitialId )
			interstitialAutoreload = false
			autoreloadInterstitialBtn:setLabel( "Enable Interstitial Autoreload" )
		else
			aatkit.startPlacementAutoReload( interstitialId )
			interstitialAutoreload = true
			autoreloadInterstitialBtn:setLabel( "Disable Interstitial Autoreload" )
		end
	end

	return true
end

local function onShowInterstitialBtnRelease()
	if initialized then
		aatkit.showPlacement( interstitialId )
	end

	return true
end

function libraryCallback( event )
    if event.id == AATKitHaveAd then
        print("AATKitHaveAd")
        print( event.placement )
    elseif event.id == AATKitNoAds then
        print("AATKitNoAds")
        print( event.placement )
    elseif event.id == AATKitShowingEmpty then
        print("AATKitShowingEmpty")
        print( event.placement )
    elseif event.id == AATKitPauseForAd then
        print("AATKitPauseForAd")
    elseif event.id == AATKitResumeAfterAd then
        print("AATKitResumeAfterAd")
    elseif event.id == AATKitUserEarnedIncentive then
        print("AATKitUserEarnedIncentive")
        print( event.placement )
    end
end

function scene:create( event )
	local sceneGroup = self.view

	-- -- Called when the scene's view does not exist.
	-- --
	-- -- INSERT code here to initialize the scene
	-- -- e.g. add display objects to 'sceneGroup', add touch listeners, etc.

 	print(aatkit.getVersion())
    version = aatkit.getVersion()
    local label = display.newText( version, 72, display.contentHeight-10, "Helvetica", 10 )
	label:setTextColor( 255, 255, 255, 255 )


	-- create a widget buttons
	initBtn = widget.newButton{
		label="Init AATKit",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onInitBtnRelease
	}
	initBtn.x = display.contentWidth*0.5
	initBtn.y = 20

	debugBtn = widget.newButton{
		label="Enable Debug Log",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onDebugBtnRelease
	}
	debugBtn.x = display.contentWidth*0.5
	debugBtn.y = 60

	shakeBtn = widget.newButton{
		label="Enable Debug Shake",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onShakeBtnRelease
	}
	shakeBtn.x = display.contentWidth*0.5
	shakeBtn.y = 100

	reloadBannerBtn = widget.newButton{
		label="Reload Banner",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onReloadBannerBtnRelease
	}
	reloadBannerBtn.x = display.contentWidth*0.5
	reloadBannerBtn.y = 140

	autoreloadBannerBtn = widget.newButton{
		label="Enable Banner Autoreload",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onAutoreloadBannerBtnRelease
	}
	autoreloadBannerBtn.x = display.contentWidth*0.5
	autoreloadBannerBtn.y = 180

	bannerAlignBtn = widget.newButton{
		label="Change Banner Alignment",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onBannerAlignBtnRelease
	}
	bannerAlignBtn.x = display.contentWidth*0.5
	bannerAlignBtn.y = 220

	reloadInterstitialBtn = widget.newButton{
		label="Reload Interstitial",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onReloadInterstitialBtnRelease
	}
	reloadInterstitialBtn.x = display.contentWidth*0.5
	reloadInterstitialBtn.y = 260

	autoreloadInterstitialBtn = widget.newButton{
		label="Enable Interstitial Autoreload",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onAutoreloadInterstitialBtnRelease
	}
	autoreloadInterstitialBtn.x = display.contentWidth*0.5
	autoreloadInterstitialBtn.y = 300

	showInterstitialBtn = widget.newButton{
		label="Show Interstitial",
		labelColor = { default={255}, over={128} },
		fontSize = 10,
		default="button.png",
		over="button-over.png",
		width=154, height=40,
		onRelease = onShowInterstitialBtnRelease
	}
	showInterstitialBtn.x = display.contentWidth*0.5
	showInterstitialBtn.y = 340
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then
		-- Called when the scene is now on screen
		--
		-- INSERT code here to make the scene come alive
		-- e.g. start timers, begin animation, play audio, etc.
	end
end

function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	if event.phase == "will" then
		-- Called when the scene is on screen and is about to move off screen
		--
		-- INSERT code here to pause the scene
		-- e.g. stop timers, stop animation, unload sounds, etc.)
	elseif phase == "did" then
		-- Called when the scene is now off screen
	end
end

function scene:destroy( event )
	local sceneGroup = self.view

	-- Called prior to the removal of scene's "view" (sceneGroup)
	--
	-- INSERT code here to cleanup the scene
	-- e.g. remove display objects, remove touch listeners, save state, etc.

	if initBtn then
		initBtn:removeSelf()	-- widgets must be manually removed
		initBtn = nil
	end
	if debugBtn then
		debugBtn:removeSelf()
		debugBtn = nil
	end
	if shakeBtn then
		shakeBtn:removeSelf()
		shakeBtn = nil
	end
	if reloadBannerBtn then
		reloadBannerBtn:removeSelf()
		reloadBannerBtn = nil
	end
	if autoreloadBannerBtn then
		autoreloadBannerBtn:removeSelf()
		autoreloadBannerBtn = nil
	end
	if bannerAlignBtn then
		bannerAlignBtn:removeSelf()
		bannerAlignBtn = nil
	end
	if reloadInterstitialBtn then
		reloadInterstitialBtn:removeSelf()
		reloadInterstitialBtn = nil
	end
	if autoreloadInterstitialBtn then
		autoreloadInterstitialBtn:removeSelf()
		autoreloadInterstitialBtn = nil
	end
	if showInterstitialBtn then
		showInterstitialBtn:removeSelf()
		showInterstitialBtn = nil
	end
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-----------------------------------------------------------------------------------------

return scene
